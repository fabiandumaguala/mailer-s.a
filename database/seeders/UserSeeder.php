<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Person;
use App\Models\User;
use App\Models\Phone;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creación del administrador

        $user = new User();
        $user->name = 'Admin A.';
        $user->email = 'admin@admin.com';
        $user->password = 'admin123';
        $user->type = 'admin';
        $user->save();

        $phone = new Phone();
        $phone->mobile_phone = '6465415515';
        $phone->save();

        $person = new Person();
        $person->name = 'Admin';
        $person->dni = '0121545454';
        $person->date_birth = '1995-10-04 22:23:00';
        $person->user()->associate($user);
        $person->city()->associate(3);
        $person->phone()->associate($phone);
        $person->save();


    }
}
