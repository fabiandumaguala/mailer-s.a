<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = array("México", "Ecuador");

        foreach ($countries as $country_name){
            $country = new Country();
            $country->name = $country_name;
            $country->save();
        }
    }
}
