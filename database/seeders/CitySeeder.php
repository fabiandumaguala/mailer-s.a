<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Aguascalientes
        $cities = array("Aguascalientes", "Asientos");

        foreach ($cities as $city_name){
            $city = new City();
            $city->name = $city_name;
            $city->code = '1000131';
            $city->state()->associate(1);
            $city->save();
        }
       
        //Azuay
        $cities = array("Cuenca", "Gualaceo");

        foreach ($cities as $city_name){
            $city = new City();
            $city->name = $city_name;
            $city->code = '1000131';
            $city->state()->associate(4);
            $city->save();
        }
    }
}
