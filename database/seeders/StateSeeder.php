<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\State;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Mexico
        $states = array("Aguascalientes", "Baja California", "Baja California Sur");

        foreach ($states as $state_name){
            $state = new State();
            $state->name = $state_name;
            $state->country()->associate(1);
            $state->save();
        }

        //Ecuador
        $states = array("Azuay", "Bolívar", "Cañar");

        foreach ($states as $state_name){
            $state = new State();
            $state->name = $state_name;
            $state->country()->associate(2);
            $state->save();
        }
    }
}
