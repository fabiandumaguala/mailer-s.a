<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('name'); //Nombre
            $table->char('dni'); //Número de Cédula
            $table->date('date_birth'); //Fecha de nacimimiento

            //Relacion con ciudad
            $table->bigInteger('city_id')->unsigned();
            // $table->foreign('city_id')->references('id')->on('cities')
            // ->onUpdate('cascade')
            // ->onDelete('cascade');

            //Relacion con usuario
            $table->bigInteger('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('users')
            // ->onUpdate('cascade')
            // ->onDelete('cascade');

            //Relacion con ciudad
            $table->bigInteger('phone_id')->unsigned();
            // $table->foreign('phone_id')->references('id')->on('phones')
            // ->onUpdate('cascade')
            // ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
