@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        {{ __('Emails') }}

                        <a href="{{ route('emails.create') }}">
                            {{ __('Nuevo') }}
                        </a>
                        
                    </div>
                </div>

                <div class="card-body">

                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Destinatario</th>
                            <th scope="col">Asunto</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Mensaje</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($emails as $email)
                          <tr>
                            <th scope="row">{{ $email->id }}</th>
                            <td>{{ $email->addressee }}</td>
                            <td>{{ $email->subject }}</td>
                            <td>{{ $email->status }}</td>
                            <td>{{ $email->message }}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      <div class="d-flex justify-content-center">
                        {{ $emails->links() }}
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
