@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            {{ __('Usuarios') }}

                            <a href="{{ route('users.create') }}">
                                {{ __('Nuevo') }}
                            </a>

                        </div>
                    </div>

                    <div class="card-body">

                        <table class="table">

                            <form method="GET" action="{{ route('users.index') }}">
                                @csrf
                                <input class="form-control" name="search" list="datalistOptions" id="exampleDataList"
                                    placeholder="Buscar por nombre, cedula, telefono, email...">
                            </form>

                            <thead>
                                <tr>
                                    <th scope="col">Identificador</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cédula</th>
                                    <th scope="col">Número Celular</th>
                                    <th scope="col">Fecha de Nacimiento</th>
                                    <th scope="col">Edad</th>
                                    <th scope="col">Ciudad</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->person->dni }}</td>
                                        <td>{{ $user->person->phone->mobile_phone }}</td>
                                        <td>{{ $user->person->date_birth }}</td>
                                        <td>{{ date('Y') - date('Y', strtotime($user->person->date_birth)) }}</td>
                                        <td>{{ $user->person->city->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <a href="{{ route('users.edit', $user->id) }}"
                                                class="bnt">Editar</a>
                                            <form method="POST" action="{{ route('users.destroy', $user->id) }}">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button class="btn-sm bg-danger text-white" type="submit">Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
