@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            {{ __('Nuevo Usuario') }}
                            <a href="{{ route('users.index') }}">
                                {{ __('Usuarios') }}
                            </a>
                        </div>
                    </div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('users.store') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="dni" class="col-md-4 col-form-label text-md-right">{{ __('Cédula') }}</label>
                                <div class="col-md-6">
                                    <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror"
                                        name="dni" value="{{ old('dni') }}" required autocomplete="dni" autofocus>

                                    @error('dni')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="mobile_phone"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Número de Celular') }}</label>
                                <div class="col-md-6">
                                    <input id="mobile_phone" type="text"
                                        class="form-control @error('mobile_phone') is-invalid @enderror" name="mobile_phone"
                                        value="{{ old('mobile_phone') }}" required autocomplete="mobile_phone" autofocus>

                                    @error('mobile_phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="date_birth"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Nacimiento') }}</label>
                                <div class="col-md-6">
                                    <input id="date_birth" type="date"
                                        class="form-control @error('date_birth') is-invalid @enderror" name="date_birth"
                                        value="{{ old('date_birth') }}" required autocomplete="date_birth" autofocus>

                                    @error('date_birth')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('País') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select" name="country" id="country_id" autocomplete="country"
                                        aria-label="Default select example">
                                        <option selected>Seleccione un país</option>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Provincia / Estado') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select" id="state_id" name="state" autocomplete="state"
                                        aria-label="Default select example">
                                        <option selected>Selecione una provincia / estado</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Ciudad') }}</label>
                                <div class="col-md-6">
                                    <select class="form-select  @error('city') is-invalid @enderror" name="city"
                                        autocomplete="city" id="city_id" aria-label="Default select example">
                                        <option selected>Seleccione una ciudad</option>
                                    </select>

                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password-confirm"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Guardar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        // Obtener provincia / estado según pais
        jQuery(document).ready(function() {
            console.log('jquery load');
            jQuery('#country_id').change(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "/states/" + $(this).val(),
                    method: 'GET',
                    success: function(states) {

                        //Asignamos los estados
                        var old = $('#state_id').data('old') != '' ? $('#state_id').data('old') :'';
                        jQuery('#state_id').empty();
                        jQuery('#state_id').append("<option selected>Selecione una provincia / estado</option>");
                        jQuery('#city_id').empty();
                        jQuery('#city_id').append("<option selected>Seleccione una ciudad</option>");
                        jQuery.each(states, function(index, value) {
                            jQuery('#state_id').append(
                                "<option value='" + value.id +"'" + (old == value.id ? 'selected' : '') + ">" + value.name +"</option>"
                            );
                        })

                    }
                });
            });
        });

        // Obtener ciudad según estado
        jQuery(document).ready(function() {
            console.log('jquery load');
            jQuery('#state_id').change(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "/cities/" + $(this).val(),
                    method: 'GET',
                    success: function(cities) {

                        //Asignamos las ciudades
                        var old = $('#city_id').data('old') != '' ? $('#city_id').data('old') :'';
                        jQuery('#city_id').empty();
                        jQuery('#city_id').append("<option selected>Seleccione una ciudad</option>");
                        jQuery.each(cities, function(index, value) {
                            jQuery('#city_id').append(
                                "<option value='" + value.id +"'" + (old == value.id ? 'selected' : '') + ">" + value.name +"</option>"
                            );
                        })

                    }
                });
            });
        });

    </script>
@endsection
