<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/

	protected $fillable = [
	    'name', 'dni', 'date_birth', 'user_id', 'city_id', 'phone_id'
	];

	public function setDniAttribute($dni){
		if($dni != 'null'){
			$this->attributes['dni'] = $dni;
		}
	}

	public function city(){
        return $this->belongsTo('App\Models\City');
    }

    public function phone(){
    	return $this->belongsTo('App\Models\Phone');
    }

	public function user(){
    	return $this->belongsTo('App\Models\User');
	}
}
