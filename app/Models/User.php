<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Encriptacion de la contraseña
     */
    public function setPasswordAttribute($password){
        if(!empty($password) && $password != 'null'){
            $this->attributes['password'] = Hash::make($password);
        }
    }

    //Relacion con persona
    public function person(){
    	return $this->hasOne('App\Models\Person');
	}

    //Relacion con emails
    public function emails(){
        return $this->hasMany('App\Models\Email');
    }

    //Buscador
    public function scopeSearch($query, $search){
		// return $query->where('name', 'LIKE', "%$search%")
		// 	// ->orWhere('quantity', 'LIKE', "%$search%")
		// 	->orWhere('price', 'LIKE', "%$search%");
		return $query->where(DB::raw("CONCAT(users.name, ' ', dni, ' ', date_birth, ' ', email , ' ', mobile_phone, ' ', cities.name )"), "LIKE", "%$search%");

	}
}
