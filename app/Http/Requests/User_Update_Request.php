<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class User_Update_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|string|max:100',
            // 'dni'         => 'required|string|max:15',
            'mobile_phone'=> 'required|string',
            'date_birth'  => 'required|string',
            'city'        => 'integer',
            // 'email'       => 'required|string|email|max:255|unique:users, email,' . $this->id,
            // 'password'    => 'string|min:8|confirmed',
        ];
    }
}
