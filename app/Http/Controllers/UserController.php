<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User_Create_Request;
use App\Http\Requests\User_Update_Request;
use Illuminate\Support\Facades\Log;

use App\Models\Country;
use App\Models\User;
use App\Models\Phone;
use App\Models\Person;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::search($request['search'])
        ->leftjoin("people",  "people.user_id",  "=", "users.id")
        ->leftjoin("phones",  "phones.id",  "=", "people.phone_id")
        ->leftjoin("cities",  "cities.id",  "=", "people.city_id")
        ->select('users.*', 'people.*')
        ->paginate(15);
   
        $users->each(function($users){
            $users->person;
            $users->person->phone;
            $users->person->city;
        });

        return view('users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create', [
            'countries' => Country::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User_Create_Request $request)
    {
        $user = new User($request->all());
        $user->save();

        $phone = new Phone($request->all());
        $phone->save();

        $person = new Person($request->all());
        $person->user()->associate($user);
        $person->city()->associate($request->city);
        $person->phone()->associate($phone);
        $person->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $user->person;
        $user->person->phone;
        $user->city;

        return view('users.edit', [
            'user' => $user,
            'countries' => Country::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User_Update_Request $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        $user->update();

        $phone = Phone::find($user->person->phone->id);
        $phone->fill($request->all());
        $phone->update();

        $person = Person::find($user->person->id);
        $person->fill($request->all());
        $person->user()->associate($user);
        $person->city()->associate($request->city);
        $person->phone()->associate($phone);
        $person->update();

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->type != 'admin'){
            $user->delete();
        }
        return redirect()->route('users.index');
    }
}
